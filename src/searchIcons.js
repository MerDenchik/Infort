//const sortArrayFileFirst = require('./sortArrayFileFirst');
const path = require('path');
const uniqArr = require('./uniqArr');

/**
 * Processing images.
 * 
 * @function searchIcons
 * @param {Array} files - Images.
 * @return {Promise <Array>} Сollected icons.
 */
module.exports = files => {
    /** Sort icons. Favicon -> Icon -> Logo -> Other. */
    // let collectedInfo = sortArrayFileFirst(files, /logo|icon/i);
    // collectedInfo = sortArrayFileFirst(collectedInfo, /favicon|favicon\.png|favicon\.svg/i);

    let logo = files.filter(function(file) {
    	let fileName = path.basename(file);
        return /logo/i.test(fileName);
    });

    let icon = files.filter(function(file) {
    	let fileName = path.basename(file);
        return /icon/i.test(fileName);
    });

    let favicon = files.filter(function(file) {
    	let fileName = path.basename(file);
        return /favicon|favicon\.png|favicon\.svg/i.test(fileName);
    });

    let collectedInfo = [...favicon, ...icon, ...logo];
    collectedInfo = uniqArr(collectedInfo);

    return collectedInfo;
}