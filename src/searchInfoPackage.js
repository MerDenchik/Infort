const jsonGet = require('oget/oget.js');

const getSafe = require('./getSafe');
const searchByKey = require('./searchByKey');

const bundlers = require('./data/bundlers.json');
const taskRunners = require('./data/taskRunners.json');
const tests = require('./data/tests.json');
const preprocessors = require('./data/preprocessors.json');

/**
 * Search for information from package.
 * 
 * @function searchInfoPackage
 * @param {String} data - Package data.
 * @param {String} projectPath - Path to package (For scripts).
 * @return {Object} Information from package.
 * @property {(String | undefined)} name 
 * @property {(String | undefined)} description 
 * @property {Array} keywords 
 * @property {(String | undefined)} version 
 * @property {(String | undefined)} license 
 * @property {(String | undefined)} homepage 
 * @property {Array} scripts 
 * @property {(String | undefined)} repository 
 * @property {(String | undefined)} author
 * @property {(String | undefined)} bugs
 * @property {Array} dependencies
 * @property {Array} devDependencies
 * @property {Array} bundlers
 * @property {Array} taskRunners
 * @property {Array} tests
 * @property {Array} preprocessors
 */
module.exports = (data, projectPath) => {
    let info = {
        name: undefined,
        description: undefined,
        keywords: [],
        version: undefined,
        license: undefined,
        homepage: undefined,
        scripts: [],
        repository: undefined,
        author: undefined,
        bugs: undefined,
        dependencies: [],
        devDependencies: [],
        bundlers: [],
        taskRunners: [],
        tests: [],
        preprocessors: []
    };

    /**
     * Safely parsed data from package.
     * @type {Object}
     */
    let projectPackage = getSafe(() => JSON.parse(data), undefined);

    /** Basic info
     * @type {(String | undefined)}
     */
    info.name = jsonGet(projectPackage, 'name', undefined);
    info.description = jsonGet(projectPackage, 'description', undefined);

    info.keywords = jsonGet(projectPackage, 'keywords', undefined);
    /** Lead to Array */
    info.keywords = Array.isArray(info.keywords) ? info.keywords : (typeof info.keywords == 'string' ? info.keywords.split(',') : undefined);

    info.version = jsonGet(projectPackage, 'version', undefined);
    info.license = jsonGet(projectPackage, 'license', undefined);
    info.homepage = jsonGet(projectPackage, 'homepage', undefined);

    let scripts = jsonGet(projectPackage, 'scripts', undefined);
    /** Lead to Array */
    if(scripts && typeof scripts === 'object') {
        for(let script in scripts) {
            let scriptName = scripts[script];
            info.scripts.push({
                name: scriptName,
                cmd: script,
                folder: projectPath ? projectPath : undefined
            });
        }
    }

    info.repository = jsonGet(projectPackage, 'repository', undefined);
    /** Lead to String */
    info.repository = typeof info.repository == 'object' ? jsonGet(info.repository, 'url', undefined) : (typeof info.repository == 'string' ? info.repository : undefined);

    info.author = jsonGet(projectPackage, 'author', undefined);
    /** Lead to String */
    info.author = typeof info.author == 'object' ? jsonGet(info.author, 'name', undefined) : (typeof info.author == 'string' ? info.author : undefined);

    info.bugs = jsonGet(projectPackage, 'bugs', undefined);
    /** Lead to String */
    info.bugs = typeof info.bugs == 'object' ? jsonGet(info.bugs, 'url', undefined) : (typeof info.bugs == 'string' ? info.bugs : undefined);

    /**
     * Get dependencies in needed format (Array).
     * 
     * @function getDependencies
     * @param {Object} package - Package contains dependencies.
     * @param {String} key - 'dependencies' | 'devDependencies'.
     * @return {Array} Dependencies.
     */
    const getDependencies = (packageData, key) => {
        let arr = [];

        /**
         * Dependencies in JSON
         * @type {Object}
         */
        let dependencies = jsonGet(packageData, key, {});

        for (let name in dependencies) {

            /**
             * Version without other semver symbols
             * @type {String}
             */
            let version = dependencies[name].replace(/[^\d\.]/g, "").replace(/\.?$/, "");

            arr.push({
                name,
                version,
                url: 'https://www.npmjs.com/package/' + name
            });
        }

        return arr;
    };

    info.dependencies = getDependencies(projectPackage, 'dependencies');
    info.devDependencies = getDependencies(projectPackage, 'devDependencies');

    /** Search tools in the developing dependencies */
    info.devDependencies.forEach((dependency) => {
        /** 
         * Dependency name
         * @type {String}
         */
        let dependencyName = dependency.name;

        /** 
         * Dependency version
         * @type {String}
         */
        let dependencyVersion = dependency.version;

        /** Search bundlers */
        let bundler = searchByKey(bundlers, dependencyName);
        if (bundler) {
            /** Add the version */
            bundler.version = dependencyVersion;

            info.bundlers.push(bundler);
        }

        /** Search task runners */
        let taskRunner = searchByKey(taskRunners, dependencyName);
        if (taskRunner) {
            /** Add the version */
            taskRunner.version = dependencyVersion;

            info.taskRunners.push(taskRunner);
        }

        /** Search test runners */
        let testRunner = searchByKey(tests, dependencyName);
        if (testRunner) {
            /** Add the version */
            testRunner.version = dependencyVersion;

            info.tests.push(testRunner);
        }

        /** Search preprocessors */
        let preprocessor = searchByKey(preprocessors.dependency, dependencyName);
        if (preprocessor) {
            info.preprocessors.push(preprocessor);
        }
    });

    return info;
};