const path = require('path');

/**
 * Sort array. Items that match pattern will be at start;
 * 
 * @function sortArrayFileFirst
 * @param {Array} arr
 * @param {Object} pattern - Pattern that array items should match.
 * @return {Array} Sorted array
 */
module.exports = (arr, pattern) => {
    return arr.sort((aFile, bFile) => {
        /**
         * Filename of first comparable file.
         * @type {String}
         */
        let aFileName = path.basename(aFile);

        /**
         * Filename of second comparable file.
         * @type {String}
         */
        let bFileName = path.basename(bFile);

        if (pattern.test(aFileName) && pattern.test(bFileName)) {
            return 0;
        }
        if (!pattern.test(aFileName)) {
            return 1;
        }
        if (!pattern.test(bFileName)) {
            return -1;
        }
    });
}