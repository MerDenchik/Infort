'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//const sortArrayFileFirst = require('./sortArrayFileFirst');
var path = require('path');
var uniqArr = require('./uniqArr');

/**
 * Processing images.
 * 
 * @function searchIcons
 * @param {Array} files - Images.
 * @return {Promise <Array>} Сollected icons.
 */
module.exports = function (files) {
    /** Sort icons. Favicon -> Icon -> Logo -> Other. */
    // let collectedInfo = sortArrayFileFirst(files, /logo|icon/i);
    // collectedInfo = sortArrayFileFirst(collectedInfo, /favicon|favicon\.png|favicon\.svg/i);

    var logo = files.filter(function (file) {
        var fileName = path.basename(file);
        return (/logo/i.test(fileName)
        );
    });

    var icon = files.filter(function (file) {
        var fileName = path.basename(file);
        return (/icon/i.test(fileName)
        );
    });

    var favicon = files.filter(function (file) {
        var fileName = path.basename(file);
        return (/favicon|favicon\.png|favicon\.svg/i.test(fileName)
        );
    });

    var collectedInfo = [].concat(_toConsumableArray(favicon), _toConsumableArray(icon), _toConsumableArray(logo));
    collectedInfo = uniqArr(collectedInfo);

    return collectedInfo;
};