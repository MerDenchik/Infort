'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var jsonGet = require('oget/oget.js');

var getSafe = require('./getSafe');
var searchByKey = require('./searchByKey');

var bundlers = require('./data/bundlers.json');
var taskRunners = require('./data/taskRunners.json');
var tests = require('./data/tests.json');
var preprocessors = require('./data/preprocessors.json');

/**
 * Search for information from package.
 * 
 * @function searchInfoPackage
 * @param {String} data - Package data.
 * @param {String} projectPath - Path to package (For scripts).
 * @return {Object} Information from package.
 * @property {(String | undefined)} name 
 * @property {(String | undefined)} description 
 * @property {Array} keywords 
 * @property {(String | undefined)} version 
 * @property {(String | undefined)} license 
 * @property {(String | undefined)} homepage 
 * @property {Array} scripts 
 * @property {(String | undefined)} repository 
 * @property {(String | undefined)} author
 * @property {(String | undefined)} bugs
 * @property {Array} dependencies
 * @property {Array} devDependencies
 * @property {Array} bundlers
 * @property {Array} taskRunners
 * @property {Array} tests
 * @property {Array} preprocessors
 */
module.exports = function (data, projectPath) {
    var info = {
        name: undefined,
        description: undefined,
        keywords: [],
        version: undefined,
        license: undefined,
        homepage: undefined,
        scripts: [],
        repository: undefined,
        author: undefined,
        bugs: undefined,
        dependencies: [],
        devDependencies: [],
        bundlers: [],
        taskRunners: [],
        tests: [],
        preprocessors: []
    };

    /**
     * Safely parsed data from package.
     * @type {Object}
     */
    var projectPackage = getSafe(function () {
        return JSON.parse(data);
    }, undefined);

    /** Basic info
     * @type {(String | undefined)}
     */
    info.name = jsonGet(projectPackage, 'name', undefined);
    info.description = jsonGet(projectPackage, 'description', undefined);

    info.keywords = jsonGet(projectPackage, 'keywords', undefined);
    /** Lead to Array */
    info.keywords = Array.isArray(info.keywords) ? info.keywords : typeof info.keywords == 'string' ? info.keywords.split(',') : undefined;

    info.version = jsonGet(projectPackage, 'version', undefined);
    info.license = jsonGet(projectPackage, 'license', undefined);
    info.homepage = jsonGet(projectPackage, 'homepage', undefined);

    var scripts = jsonGet(projectPackage, 'scripts', undefined);
    /** Lead to Array */
    if (scripts && (typeof scripts === 'undefined' ? 'undefined' : _typeof(scripts)) === 'object') {
        for (var script in scripts) {
            var scriptName = scripts[script];
            info.scripts.push({
                name: scriptName,
                cmd: script,
                folder: projectPath ? projectPath : undefined
            });
        }
    }

    info.repository = jsonGet(projectPackage, 'repository', undefined);
    /** Lead to String */
    info.repository = _typeof(info.repository) == 'object' ? jsonGet(info.repository, 'url', undefined) : typeof info.repository == 'string' ? info.repository : undefined;

    info.author = jsonGet(projectPackage, 'author', undefined);
    /** Lead to String */
    info.author = _typeof(info.author) == 'object' ? jsonGet(info.author, 'name', undefined) : typeof info.author == 'string' ? info.author : undefined;

    info.bugs = jsonGet(projectPackage, 'bugs', undefined);
    /** Lead to String */
    info.bugs = _typeof(info.bugs) == 'object' ? jsonGet(info.bugs, 'url', undefined) : typeof info.bugs == 'string' ? info.bugs : undefined;

    /**
     * Get dependencies in needed format (Array).
     * 
     * @function getDependencies
     * @param {Object} package - Package contains dependencies.
     * @param {String} key - 'dependencies' | 'devDependencies'.
     * @return {Array} Dependencies.
     */
    var getDependencies = function getDependencies(packageData, key) {
        var arr = [];

        /**
         * Dependencies in JSON
         * @type {Object}
         */
        var dependencies = jsonGet(packageData, key, {});

        for (var name in dependencies) {

            /**
             * Version without other semver symbols
             * @type {String}
             */
            var version = dependencies[name].replace(/[^\d\.]/g, "").replace(/\.?$/, "");

            arr.push({
                name: name,
                version: version,
                url: 'https://www.npmjs.com/package/' + name
            });
        }

        return arr;
    };

    info.dependencies = getDependencies(projectPackage, 'dependencies');
    info.devDependencies = getDependencies(projectPackage, 'devDependencies');

    /** Search tools in the developing dependencies */
    info.devDependencies.forEach(function (dependency) {
        /** 
         * Dependency name
         * @type {String}
         */
        var dependencyName = dependency.name;

        /** 
         * Dependency version
         * @type {String}
         */
        var dependencyVersion = dependency.version;

        /** Search bundlers */
        var bundler = searchByKey(bundlers, dependencyName);
        if (bundler) {
            /** Add the version */
            bundler.version = dependencyVersion;

            info.bundlers.push(bundler);
        }

        /** Search task runners */
        var taskRunner = searchByKey(taskRunners, dependencyName);
        if (taskRunner) {
            /** Add the version */
            taskRunner.version = dependencyVersion;

            info.taskRunners.push(taskRunner);
        }

        /** Search test runners */
        var testRunner = searchByKey(tests, dependencyName);
        if (testRunner) {
            /** Add the version */
            testRunner.version = dependencyVersion;

            info.tests.push(testRunner);
        }

        /** Search preprocessors */
        var preprocessor = searchByKey(preprocessors.dependency, dependencyName);
        if (preprocessor) {
            info.preprocessors.push(preprocessor);
        }
    });

    return info;
};