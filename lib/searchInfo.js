'use strict';

var fs = require('fs');
var path = require('path');
var isObjectEmpty = require('./isObjectEmpty');

/**
 * Processing files.
 * 
 * @function searchInfo
 * @param {Array} files - Files with information.
 * @param {Function} process - File processing function for searching information.
 * @return {Array} Сollected information from files.
 */
module.exports = function (files, process) {

    /**
     * Collected information from files
     * 
     * @type {Array}
     */
    var collectedInfo = files.map(function (filePath) {
        /**
         * Data package.
         * @type {String}
         */
        var data = fs.readFileSync(filePath, 'utf-8');

        /** Search information from file */
        return process(data, path.dirname(filePath));
    });

    /** Remove objects without any information */
    collectedInfo = collectedInfo.filter(function (obj) {
        return !isObjectEmpty(obj);
    });

    return collectedInfo;
};